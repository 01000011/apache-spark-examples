resolvers ++= Seq("Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
                  "Apache HBase" at "https://repository.apache.org/content/repositories/releases")

addSbtPlugin("org.xerial.sbt" % "sbt-pack" % "0.7.9")