package com.c.examples.spark.streaming.file_system

import java.time.ZonedDateTime

import com.typesafe.config.ConfigFactory
import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.hadoop.hbase.mapred.TableOutputFormat
import org.apache.hadoop.mapred.JobConf
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.{Seconds, StreamingContext}

/**
  * Created by Courtney Harleston on 3/14/2016.
  */

object DailySalesDriver {
  def main(args: Array[String]) {
    val configuration = ConfigFactory.load("spark.properties")
    val sparkConf = new SparkConf().setAppName("DailySales").setMaster(configuration.getString("master"))
    val streamingContext = new StreamingContext(sparkConf, Seconds(2))
    val salesData = streamingContext.textFileStream(configuration.getString("dir"))
    val jobConfig = new JobConf(HBaseConfiguration.create())
    jobConfig.setOutputFormat(classOf[TableOutputFormat])
    jobConfig.set(TableOutputFormat.OUTPUT_TABLE, Persistence.salesTable)
    DailySales.calculate(streamingContext, salesData) { dailySales: RDD[DailySales] =>
      dailySales.mapPartitions({ fp =>
        fp.map(f => {
          Persistence.createDailyInsertStatement(f.storeName, ZonedDateTime.parse(f.iso8601Date), f.total)
        })
      }).saveAsHadoopDataset({
        jobConfig
      })
    }
    streamingContext.start()
    streamingContext.awaitTermination()
  }
}
