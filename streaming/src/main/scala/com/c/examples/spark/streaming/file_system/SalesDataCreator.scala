package com.c.examples.spark.streaming.file_system

import java.io.PrintWriter
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.UUID

import scala.util.Random

/**
  * Created by Courtney Harleston on 3/14/2016.
  */
object TransactionType {
  val Credit: String = "CREDIT"
  val Purchase: String = "PURCHASE"

  def all(): Array[String] = {
    Array(Credit, Purchase)
  }
}

object ItemType {
  val SourPatchKids: String = "SOUR PATCH KIDS"
  val Coke: String = "COKE"
  val Apples: String = "APPLES"
  val Bread: String = "BREAD"
  val Potatoes: String = "POTATOES"

  val Cookies: String = "COOKIES"
  val Salad: String = "SALAD"
  val Chicken: String = "CHICKEN"
  val Shoes: String = "SHOES"
  val Oranges: String = "ORANGES"

  val KanyeWestCD: String = "KANYE WEST CD"
  val IceCream: String = "ICE CREAM"
  val CirocVodka: String = "CIROC VODKA"
  val EffenVodka: String = "EFFEN VODKA"
  val ConjureVodka: String = "CONJURE VODKA"

  val Steak: String = "STEAK"
  val Shrimp: String = "SHRIMP"
  val Scallops: String = "SCALLOPS"
  val Socks: String = "SOCKS"
  val Water: String = "WATER"

  def all() : Array[String] = {
    Array[String](SourPatchKids, Coke, Apples, Bread, Potatoes, Cookies, Salad, Chicken, Shoes, Oranges, KanyeWestCD,
      IceCream, CirocVodka, EffenVodka, ConjureVodka, Steak, Shrimp, Scallops, Socks, Water)
  }
}

object Store{
  def all() = Array("target store #1,East,Ga,Atlanta,30303",
  "target store #2,North,NY,New York,100001",
  "target store #3,West,Ca,Los Angeles,90210",
  "target store #4,East,Ga,Atlanta,30310",
  "target store #5,East,Ga,Atlanta,30326",
  "target store #6,West,Ca,Los Angeles,90310",
  "target store #7,West,Ca,Los Angeles,90213",
  "target store #8,North,NY,New York,100002")
}

object SalesDataCreator {
  val maxItems: Short = 20
  val stores = Store.all()
  val items = ItemType.all()
  val transactionTypes = TransactionType.all()

  def createItem(): String = {
    val ran = (Random.nextDouble() * 100).formatted("%1.2f").toDouble
    val transactionType: String = transactionTypes(Random.nextInt(transactionTypes.length))
    val amount: Double = if (transactionType.equals(TransactionType.Credit)) ran * -1 else ran * 1
    val name: String = items(Random.nextInt(items.length))
    s"$name,$amount,$transactionType,${ZonedDateTime.now().format(DateTimeFormatter.ISO_INSTANT)}"
  }

  def createItemData(path : String, numOfTrans : Long): Unit = {
    val printWriter = new PrintWriter(path)
    var j = 0

    def randomStore(): String = {
      stores(Random.nextInt(stores.length)).split(",")(0)
    }
    while (j < numOfTrans) {
      val transId = UUID.randomUUID().toString
      val store = randomStore()
      for (a <- 1 to Random.nextInt(maxItems)) {
        printWriter.write(s"$transId,$store,${createItem()}\n")
      }
      j += 1
    }
    printWriter.close()
  }

  def createStoreData(path : String): Unit = {
    val printWriter = new PrintWriter(path)
    stores.foreach(store => {
      printWriter.write(s"$store\n")
    })
    printWriter.close()
  }

  def main(args: Array[String]): Unit = {
    createItemData(args(0), args(1).toLong)
  }
}
 