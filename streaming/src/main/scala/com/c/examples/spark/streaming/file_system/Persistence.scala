package com.c.examples.spark.streaming.file_system

import java.time.ZonedDateTime

import org.apache.hadoop.hbase.client.Put
import org.apache.hadoop.hbase.io.ImmutableBytesWritable
import org.apache.hadoop.hbase.util.Bytes

/**
  * Created by Courtney Harleston on 3/14/2016.
  */
object Persistence {
  val salesTable = "metric:sales"
  val dailyColumnFamily = Bytes.toBytes("d")

  def createDailyInsertStatement2(storeDateAmount: ((String, ZonedDateTime), Double)):
  (ImmutableBytesWritable, Put) = {
    createStatement(storeDateAmount._1._1, storeDateAmount._1._2, storeDateAmount._2)
  }

  def createDailyInsertStatement(storeName: String, zonedDateTime: ZonedDateTime, total: Double):
  (ImmutableBytesWritable, Put) = {
    createStatement(storeName, zonedDateTime, total)
  }

  private def createRowKey(zonedDateTime: ZonedDateTime): String = {
    s"${zonedDateTime.getYear}${zonedDateTime.getDayOfMonth}${zonedDateTime.getMonthValue}"
  }

  private def createStatement(store: String, zonedDateTime: ZonedDateTime, total: Double):
  (ImmutableBytesWritable, Put) = {
    val rowKey = Bytes.toBytes(createRowKey(zonedDateTime))
    val put: Put = new Put(rowKey)
    put.addColumn(dailyColumnFamily, Bytes.toBytes(store), Bytes.toBytes(total))
    (new ImmutableBytesWritable(rowKey), put)
  }
}
