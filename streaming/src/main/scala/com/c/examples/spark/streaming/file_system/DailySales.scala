package com.c.examples.spark.streaming.file_system

import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.dstream.DStream

/**
  * Created by Courtney Harleston on 3/14/16.
  */
case class DailySales(storeName: String, iso8601Date : String, total:Double)
object DailySales {
  val delimiter = ","
  type DailySalesHandler = (RDD[DailySales]) => Unit

  private def create(line: String): ((String, String), Double) = {
    val transaction = line.split(delimiter)
    ((transaction(1), transaction(5)), transaction(3).toDouble)
  }

  def calculateDaily(v1: Double, v2: Double): Double = {
    v1 + v2
  }

  def calculate(streamingContext: StreamingContext, salesData: DStream[String])
               (dailySalesHandler: DailySalesHandler): Unit = {
    salesData.map(create).
      foreachRDD(rdd => {
        dailySalesHandler(rdd.reduceByKey(calculateDaily).map({
          case (storeNameIso8601Date: (String, String), total: Double) =>
            DailySales(storeNameIso8601Date._1, storeNameIso8601Date._2, total)
        }))
      })
  }
}
