name := "examples-spark-streaming"

version := "1.0"

organization := "com.c.examples.spark.streaming"

scalaVersion := "2.11.8"

packAutoSettings

libraryDependencies ++= {
  val typeSafeVer         = "1.2.1"
  val sparkVer            = "1.6.1"
  val hBaseVer            = "1.2.0"
  Seq(
    "org.apache.spark" %% "spark-streaming" % sparkVer,
    "com.typesafe"      % "config"          % typeSafeVer,
    ("org.apache.hbase" % "hbase-server"    % hBaseVer)
      excludeAll ExclusionRule(organization = "org.mortbay.jetty"),
    "org.apache.hbase"  % "hbase-common"    % hBaseVer
  )
}